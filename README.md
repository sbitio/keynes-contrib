# Keynes Contrib

Jenkins pipelines, Ansible playbooks and other resources for [Keynes](https://gitlab.com/sbitio/keynes).

## Contents of this repository

...

## Development

Development happens on [Gitlab](https://gitlab.com/sbitio/keynes-contrib).

Please log issues for any bug report, feature or support request.

## License

MIT License, see LICENSE file

## Contact

Please use the contact form on http://sbit.io
