#!groovy

def call() {
    echo "WHY ARE YOU CALLING THIS FUNCTION!??! - This is a hacky workaround for https://issues.jenkins.io/browse/JENKINS-62961"
}

// vi:syntax=groovy
